﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncApp
{
    class Program
    {
        public static double x, y;

        static void Main(string[] argv)
        {
            PullIn();
            CheckIn();
            Console.ReadLine();
        }

        public static void PullIn()
        {
            /*формула окружности: (x-a)^2 + (y-b)^2 = R^2
              где (x;y) - точка, которая пренадлежит окружности, 
              а (а;b) - точка центра круга. R - радиус круга
              В нашем варианте точка центра - начало коордатых осей.
              После преобразований формула будет иметь вид:
              x^2 + y^2 = R^2*/


            while (true)
            {
                Console.WriteLine("X : (-2,2); Y : (-2,2)");
                Console.WriteLine("Please enter coordinates of the dot (X): ");
                x = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Please enter coordinates of the dot (Y): ");
                y = Convert.ToDouble(Console.ReadLine());
                if(x <= 2 && x >= -2 && y <= 2 && y >= -2)
                {
                    break;
                }
            }
        }

        static void CheckIn()
        {
            double radius = 4;
            if(Math.Pow(x,2) + Math.Pow(y,2) > radius && x < 0 && y > 0 && Math.Abs(x) > Math.Abs(y))
            {
                Console.WriteLine("Points are in the area.");
            }
            else
            {
                Console.WriteLine("Point is not in the area.");
            }
        }
    }
}
