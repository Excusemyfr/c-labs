﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1Lab2task10var
{
    class Program
    {
        public static int[] mass1;
        public static int[] mass2;
        public static int[] resultMass;

        static void Main(string[] args)
        {
            ArrayCreation();
            ResultMass();
            OputOnScreen();
            Console.ReadLine();
        }
        static void ArrayCreation()
        {
            mass1 = new int[6] {1, 2, 4, 5, 3, 8};
            mass2 = new int[6] {10, 21, 24, 55, 43, 12};
        }

        static void ResultMass()
        {
            resultMass = new int[20];
            int count = 0;

            for (int i = 0; i < 6; i++)
            {
                resultMass[count] = mass2[i];
                count++;
                resultMass[count] = mass1[i];
                count++;
            }
        }

        static void OputOnScreen()
        {
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine(" {0} number of array: {1}", i + 1, resultMass[i]);
            }
        }
    }
}
